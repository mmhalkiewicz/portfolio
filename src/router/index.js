import { createRouter, createWebHistory } from "vue-router";
import MainPage from "../views/MainPage.vue";
import CreditCardProject from "@/views/Projects/CreditCardProject";
import SplitterProject from "@/views/Projects/SplitterProject";
import CV from "@/views/Projects/CV";
import PomodoroTimer from "@/views/Projects/PomodoroTimer";
import CountdownTimer from "@/views/Projects/CountdownTimer";
import CalculatorProject from "@/views/Projects/CalculatorProject";
import CanvasProject from "@/views/Projects/CanvasProject";

const routes = [
  {
    path: "/",
    name: "home",
    component: MainPage,
  },
  {
    path: "/my-projects/credit-card-project",
    component: CreditCardProject,
    meta: { showNavbar: false },
  },
  {
    path: "/my-projects/splitter-project",
    component: SplitterProject,
    meta: { showNavbar: false, showFooter: false },
  },
  {
    path: "/cv",
    component: CV,
    meta: { showNavbar: false, showFooter: false },
  },
  {
    path: "/my-projects/pomodoro-timer",
    component: PomodoroTimer,
    meta: { showNavbar: false, showFooter: false },
  },
  {
    path: "/my-projects/countdown-timer",
    component: CountdownTimer,
    meta: { showNavbar: false, showFooter: false },
  },
  {
    path: "/my-projects/calculator",
    component: CalculatorProject,
    meta: { showNavbar: false, showFooter: false },
  },
  {
    path: "/my-projects/canvas",
    component: CanvasProject,
    meta: { showNavbar: false, showFooter: false },
  },
];
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return { el: to.hash };
    }
    if (savedPosition) {
      return savedPosition;
    }
    return { top: 0 };
  },
});

export default router;
